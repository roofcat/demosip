<?php

function dateSpanishToEnglish ($date)
{
    if($date)
    {
        $fecha=$date;
        $hora="";
        
        $fechaHora=explode(" ",$date);
        if(count($fechaHora)==2)
        {
            $fecha=$fechaHora[0];
            $hora=$fechaHora[1];
        }
        
        $values=preg_split('/(\/|-)/',$fecha);
        if(count($values)==3)
        {
            if($hora && count(explode(":",$hora))==3)
            {
                $hora=explode(":",$hora);
                return date("Y-m-d H:i:s",mktime($hora[0],$hora[1],$hora[2],$values[1],$values[0],$values[2]));
            }else{
                return date("Y-m-d",mktime(0,0,0,$values[1],$values[0],$values[2]));
            }
        }
    }
    return "";
}