<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

require_once 'dateUtils.php';

$client = new Google_Client;
$client->setClientId('874571669699-6kl6tjltu685mm9gnla796ig492194lm.apps.googleusercontent.com');
$client->setClientSecret('dRPLynZyVcm97PzSfJzfaWhm');
$client->setRedirectUri('http://localhost/demosip/web/index.php/oauth2callback');
$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email',
                        'https://www.googleapis.com/auth/userinfo.profile',
                        'https://www.googleapis.com/auth/calendar',
                        'https://www.googleapis.com/auth/calendar.readonly'));

// esta variable permite añadir atributo refresh_token en el objeto token
$client->setAccessType('offline');

$oauth = new Google_Service_Oauth2($client);
$calendar = new Google_Service_Calendar($client);

/*
* Metodo que permite refrescar el token en cada cambio de url
*/
$app->before(function() use ($app, $client) {
    if ($app['session']->has('token')) {
        // llamar token para refrescarlo y usar el $client
        $access_token = $app['session']->get('token');
        $refresh_token = json_decode($access_token['access_token'])->refresh_token;

        $client->refreshToken($refresh_token);

    } else {
        return;
    }
});

$app->get('/oauth2callback', function(Request $request) use ($app, $client, $oauth) {
    // validar si esta recibiendo el parametro code
    if ($request->get('code')) {
        $client->authenticate($request->get('code'));
        $token = $client->getAccessToken();
        // guardar en sesion el access_token (para no solicitarlo en cada peticion)
        $app['session']->set('token', array('access_token' => $token));
        //$access_token = $app['session']->get('token');
        //return $access_token['access_token'];

        //guardar valores en sesion del usuario autenticado
        $user = $oauth->userinfo->get();
        $app['session']->set('user', array('email' => $user['email'],
                                            'picture' => $user['picture'],
                                            'fullName' => $user['name']));
    }
    return $app->redirect($app['url_generator']->generate('home'));
})
->bind('oauth2callback');

$app->get('/logout', function () use ($app, $client) {
    $token = json_decode($app['session']->get('token'))->access_token;
    $client->revokeToken($token);
    $app['session']->remove('token');
    $app['session']->clear();
    return $app->redirect($app['url_generator']->generate('home'));
})
->bind('logout');

$app->get('/', function () use ($app, $client) {
    // validar si hay token es porque esta autentificado el usuario

    if ($app['session']->has('token')) {
        $user_data = $app['session']->get('user');
        $user_email = $user_data['email'];
        $user_name = $user_data['fullName'];
        $user_picture = $user_data['picture'];

        return $app['twig']->render('profile.html', array(
            'user_email' => $user_email,
            'user_name' => $user_name,
            'user_picture' => $user_picture
        ));
    } else {
        // si no se envia al usuario a la pantalla de autentificacion
        $url = $client->createAuthUrl();
        return $app['twig']->render('index.html', array('url' => $url));
    }
})
->bind('home');

$app->get('/calendar/add', function () use ($app, $client, $calendar) {
    if ($app['session']->has('token')) {
        $user_data = $app['session']->get('user');
        $user_email = $user_data['email'];
        /*
        // llamar token para refrescarlo y usar el $client
        $access_token = $app['session']->get('token');
        $refresh_token = json_decode($access_token['access_token'])->refresh_token;
        // listar todos los calendarios del usuario
        $client->refreshToken($refresh_token);
        */
        $calendarList = $calendar->calendarList->listCalendarList();

        /*
        foreach ($calendarList->getItems() as $calendarListEntry) {
            print_r($calendarListEntry->id." - "
                    .$calendarListEntry->accessRole." - "
                    .$calendarListEntry->summary);
            print("<br><br>");
        }
        break;
        */

        $context = array(
            'user_email' => $user_email,
            'cal_list' => $calendarList,
        );
        return $app['twig']->render('create_event.html', $context);
    } else {
        $url = $client->createAuthUrl();
        return $app['twig']->render('index.html', $context);
    }    
})
->bind('addevent');

$app->post('/calendar/add', function (Request $request) use ($app, $client, $calendar) {
    $user_data = $app['session']->get('user');
    $user_email = $user_data['email'];

    // recibiendo parametros post
    // https://developers.google.com/google-apps/calendar/v3/reference/events/insert
    $nombre = $request->get('nombre');
    $descripcion = $request->get('descripcion');
    $lugar = $request->get('lugar');
    $fechaInicio = $request->get('fechaInicio');
    $horaInicio = $request->get('horaInicio');
    $fechaFin = $request->get('fechaFin');
    $horaFin = $request->get('horaFin');
    $calendario = $request->get('calendario');

    try {
        // instancia y creacion del evento
        $event = new Google_Service_Calendar_Event;
        $event->setSummary($nombre);
        $event->setDescription($descripcion);
        $event->setLocation($lugar);

        // instancia de objetos de tiempo fecha inicio
        // formato de fecha obligatorio es '2011-06-03T10:00:00.000-03:00'
        // YYYY-MM-DD + T + hh:mm:ss.000 + zona horaria (Chile -03:00)
        $start = new Google_Service_Calendar_EventDateTime;
        $startDate = dateSpanishToEnglish($fechaInicio)."T".$horaInicio.":00.000-03:00";
        $start->setDateTime($startDate);
        $event->setStart($start);
        
        // instancia de objetos de tiempo fecha fin
        $end = new Google_Service_Calendar_EventDateTime;
        $endDate = dateSpanishToEnglish($fechaFin)."T".$horaFin.":00.000-03:00";
        $end->setDateTime($endDate);
        $event->setEnd($end);

        // instancia objeto asistentes
        /* en el caso de agregar invitados a un evento se instancia un objeto EventAttendee

        $attendee1 = new Google_Service_Calendar_EventAttendee;
        attendee1->setEmail('usuario1@sip.cl');

        $attendee2 = new Google_Service_Calendar_EventAttendee;
        attendee2->setEmail('usuario2@sip.cl');

        // se agrega cada invitado a un array
        $attendees = array($attendee1, $attendee2);

        y se setea el array al objeto evento
        $event->attendees = attendees;
        */

        // se crea el evento con el metodo insert
        $createdEvent = $calendar->events->insert($calendario, $event);
        //return $createdEvent->getId();
        $context = array(
            'user_email' => $user_email,
            'success' => TRUE,
            'message' => "Felicidades",
            'body' => "Evento creado exitosamente."
        );
        return $app['twig']->render('create_event_post.html', $context);
    } catch (Exception $e) {
        //return $e;
        $context = array(
            'user_email' => $user_email,
            'success' => FALSE,
            'message' => "Lo sentimos",
            'body' => "La creación del Evento ha fallado."
        );
        return $app['twig']->render('create_event_post.html', $context);
    }

})->bind('paddevent');

$app->get('/calendar/list', function () use ($app, $client, $calendar) {
    $user_data = $app['session']->get('user');
    $user_email = $user_data['email'];

    // listar eventos del calendario principal de la cuenta google del usuario
    try {
        $events = $calendar->events->listEvents('primary');    
    } catch (Exception $e) {
        return $e;
    }

    $context = array(
        'user_email' => $user_email,
        'events' => $events->getItems(),
    );
    
    return $app['twig']->render('list_event.html', $context);
})->bind('listevents');

// function por defect de silex, No tocar!!!
$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    } 

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});